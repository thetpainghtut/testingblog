<?php

namespace App\Http\Controllers\Api;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Http\Resources\PostResource;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::latest()->get();
        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //I am not doing validation here

        if($request->get('photo'))
       {
          $image = $request->get('photo');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('photo'))->save(public_path('images/').$name);
          // $request->get('photo')->move(public_path('images/').$name);
        }
       $name = '1545647944.jpg';
        $post = new Post();
        $post->title = $request->title;
        $post->body = $request->body;
        $post->photo = 'images/'.$name;
        $post->category_id = $request->categoryid;
        $post->user_id = auth()->id();
        $post->save();
        
        // return new PostResource($post);
        return response()->json(['success' => 'You have successfully uploaded a post!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return new PostResource(Post::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //I am doing validation here
        $post = Post::findOrFail($id);
        $post->title = $request->title;
        $post->post_content = $request->post_content;
        $post->email = $request->email;
        $post->author = $request->author;
        $post->save();
        
        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        $post->delete();
        
        return new PostResource($post);
    }
}
