<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index', 'show']]);
    }

    public function index()
    {
        // Eloquent Model 
        // $posts = Post::all();

        // Query Builder get all posts
        // $posts = DB::table('posts')->get();
        // foreach ($posts as $post) {
        //     echo $post->title;
        // }

        // Get one row
        // $user = DB::table('users')->where('name', 'Thet Paing Htut')->first();
        // echo $user->name;

        // get one column you need
        // $email = DB::table('users')->where('name', 'Thet Paing Htut')->value('email');
        // echo $email;

        // dd();

        // $posts = DB::table('posts')
        //     ->join('users', 'posts.user_id', '=', 'users.id')
        //     ->join('categories', 'posts.category_id', '=', 'categories.id')
        //     ->select('posts.*', 'users.name', 'categories.category_name')
        //     ->get();
        $postQuery = Post::query();
        $postQuery->latest();

        if ($cat_id = request('cat_id')) {
            $postQuery->where('category_id',$cat_id);
        }

        $posts = $postQuery->get();

        $categories = Category::all();
        return view('blog',compact('posts','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        return view('posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'title' => 'required|min:5',
            "categoryid" => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'body' => 'required|min:10'
        ]);

        $imageName = time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'), $imageName);

        $fullImg = '/images/'.$imageName;

        Post::create([
            'title' => request('title'),
            'category_id' => request('categoryid'),
            'photo' => $fullImg,
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

        // dd($request);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        // dd($post);
        return view('blogpost',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $categories  = Category::all();
        return view('posts.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate(request(),[
            'title' => 'required|min:5',
            "categoryid" => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'body' => 'required|min:10'
        ]);

        $imageName = time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'), $imageName);

        $fullImg = '/images/'.$imageName;

        $id = request('editid');

        $post = Post::find($id);
        $post->title = request('title');
        $post->category_id = request('categoryid');
        $post->photo = $fullImg;
        $post->body = request('body');
        $post->user_id = auth()->id();         
        $post->save();
        // dd($request);
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        foreach($post->comments as $commet):
            $commet->delete();
        endforeach;
        $post->delete();
        return redirect('/')->with('msg','sfhsdfhsdf');
    }
}
