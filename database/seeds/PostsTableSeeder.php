<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        # Lets create 150 random posts
        $posts = factory(\App\Post::class, 10)->create();
    }

    // private function getRandomUserId() {
    //     $user = \App\User::inRandomOrder()->first();
    //     return $user->id;
    // }
}
