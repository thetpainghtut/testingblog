@extends('layouts.template')

@section('content')
	<div class="col-md-8">
		<form method="post" action="/addcategory" class="my-3">
			@csrf
			<div class="form-group">
				<label>Category Name:</label>
				<input type="text" name="categoryname" class="form-control {{ $errors->has('categoryname') ? ' is-invalid' : '' }}" >
				@if ($errors->has('categoryname'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('categoryname') }}</strong>
                    </span>
                @endif
			</div>

			<div class="form-group">
				<input type="submit" name="btnsubmit" class="btn btn-primary" value="Add">
			</div>
		</form>
	</div>
@endsection