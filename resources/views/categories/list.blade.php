@extends('layouts.template')

@section('content')
	<div class="col-md-8">
		<div class="title my-4">
			<h4 class="d-inline">Category Lists</h4>
			<a href="create" class="float-right btn btn-primary">Add Category</a>
		</div>
		<table class="table">
			<thead class="table-dark">
				<tr>
					<th>No</th>
					<th>Category Name</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($categories as $category)
				<tr>
					<td>{{ $category->id }}</td>
					<td>{{ $category->category_name }}</td>
					<td><a href="#" class="btn btn-warning">Edit</a></td>
					<td><a href="#" class="btn btn-danger">Delete</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection