@extends('layouts.template')

@section('content')
	<div class="col-md-8">
		@if(count($errors))
		  	<div class="alert alert-danger">
			    <ul>
			      @foreach($errors->all() as $error)
			        <li>{{ $error }}</li>
			      @endforeach
			    </ul>
		  	</div>
		@endif

		<form method="post" action="/post/edit" enctype="multipart/form-data" class="my-3">
			<input type="hidden" name="editid" value="{{ $post->id }}">
			@csrf
			<div class="form-group">
				<label>Post title:</label>
				<input type="text" name="title" class="form-control" value="{{ $post->title }}">
			</div>

			<div class="form-group">
				<label>Categories:</label>
				<select name="categoryid" class="form-control">
					<option value="">Choose Category name</option>
					@foreach($categories as $category)
						<option value="{{ $category->id }}" 
							@if($category->id == $post->category_id)
							{{ 'selected' }}
							@endif
							>{{ $category->category_name }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label>Photo:</label>
				<input type="file" name="photo" class="form-control-file" >
				<img src="{{ $post->photo }}" width="100px">
			</div>

			<div class="form-group">
				<label>Post Body:</label>
				<textarea name="body" class="form-control" id="summernote">{!! $post->body !!}</textarea>
			</div>

			<div class="form-group">
				<input type="submit" name="btnsubmit" class="btn btn-primary" value="Update">
			</div>
		</form>
	</div>
@endsection