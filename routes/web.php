<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Default route
// Route::get('/', function () {
// 	// $gretting = "Hello !";
//     // return view('welcome')->with('gretting', 'Victoria!');
//     // return 'Hello Laravel';
//     // return view('welcome');
// });

// Blog Home
Route::get('/','PostController@index');

// Blog Post
Route::get('post/{post}','PostController@show');

// Upload Post
Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');
Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::post('/post/edit','PostController@update');

// Comments
Route::post('/comment','CommentController@store');

// Categories
Route::get('/categories','CategoryController@index');
Route::get('/create','CategoryController@create');
Route::post('/addcategory','CategoryController@store');

// Authentication scaffolding
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// For Admin Table Auth
Route::get('admin-login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin-login', 'Auth\AdminLoginController@login');
Route::post('admin-logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
Route::get('/backend','AdminController@index')->name('backend');